#!/usr/local/bin/python3

from datetime import datetime, timedelta
from pprint import pprint
from itertools import filterfalse
import gitlab
import os
import sys
from pprint import pprint

cfg = os.getenv("PYTHON_GITLAB_CFG")
ci = os.getenv("CI")

gl = gitlab.Gitlab.from_config('default', [cfg])
gl.auth()

#
# Various configurable whitelists
#
whitelist_usernames = {
    'ghost',
    'support-bot',
    'bot-victor'
}

blacklist_names = {
    'sedayubet online',
    'Sedayubet Online'
}

whitelist_bios = {
    'http://github.com/syndbg/',
    'Github: https://github.com/andrei-pavel'
}

whitelist_urls = {
    'https://www.techsolvency.com/',
    'https://andreipavel.go.ro',
    'https://candrews.integralblue.com',
    'https://www.huque.com/',
    'https://www.robert-scheck.de',
    'https://dnsworkshop.org'
}

whitelist_domains = [
    '@isc.org',
    '@gitlab.isc.org'
]

greylist_domains = [
    '@hotmail.com',
    '@outlook.com',
    '@gmail.com'
]

social_networks = [
    'twitter',
    'skype',
    'linkedin'
]

#
# 'confirmed_at': '2019-06-21T08:15:00.376Z',
# 'created_at': '2019-06-21T08:15:00.755Z',
# 'current_sign_in_at': '2019-06-21T08:15:00.794Z',
# 'last_activity_on': '2019-06-21',
# 'last_sign_in_at': '2019-06-21T08:15:00.794Z',
#
fmt_iso_time = '%Y-%m-%dT%H:%M:%S.%fZ'
fmt_iso_date = '%Y-%m-%d'

now = datetime.now()
idle_time = now - timedelta(days=1)
kill_time = now - timedelta(days=7)

grey_idle_time = now - timedelta(hours=4)
grey_kill_time = now - timedelta(hours=8)


def user_ptime(user, attr, fmt):
    value = user.attributes[attr]
    return datetime.strptime(value, fmt) if value is not None else None


def after(a, b):
    return (a is not None) and (a > b)


#
# Collect all of the pages of users from the GL API
#
def get_all_users():
    return gl.users.list(all=True, two_factor='disabled')


from pprint import pprint

#
# Filter for users that we'll definitely retain
#
def is_good_user(user):

    username = user.attributes['username']

    if user.bot:
        print(f"User {username} is a bot")
        return True

    try:
        trusted_by = user.customattributes.get('trusted_by')
        value = trusted_by.attributes['value']
        if value is not None:
            print(f"User {username} was trusted by {value}")
            return True
    except:
        pass

    name = user.attributes['name']
    email = user.attributes['email']
    state = user.attributes['state']

    # Don't delete users with 2fa enabled
    assert not user.attributes['two_factor_enabled']

    # Don't delete internal users
    if not user.attributes['external']:
        return True

    # Don't delete users with increased project limit
    if user.attributes['projects_limit'] > 0:
        return True

    return False


#
# Filter for customised white-listing of user-specific attributes
#
def is_whitelisted(user):

    username = user.attributes['username']
    name = user.attributes['name']
    email = user.attributes['email']
    state = user.attributes['state']

    # Usernames
    if username in whitelist_usernames:
        return True

    # Email addresses
    for domain in whitelist_domains:
        if email.endswith(domain) and state == 'active':
            return True

    return False


#
# Filter for customised grey-listing of user-specific attributes
#
def is_greylisted(user):

    username = user.attributes['username']
    name = user.attributes['name']
    email = user.attributes['email']

    # Email addresses
    for domain in greylist_domains:
        if email.endswith(domain):
            return True

    return False


def is_recently_created(user, time):
    created_at = user_ptime(user, 'created_at', fmt_iso_time)
    return after(created_at, time)


def has_social_network(user):
    r = False
    for social_network in social_networks:
        v = user.attributes[social_network]
        r = r or (v != '')
    return r


def sanitize_user(user):
    user.bio = None
    user.location = None
    user.organization = None
    user.linkedin = ''
    user.twitter = ''
    user.skype = ''
    user.website_url = ''
    user.save()


def has_status(user):
    try:
        status = user.status.get()
        username = user.attributes['username']
        message = status.attributes['message']
        emoji = status.attributes['emoji']
        r = status.attributes['message'] is not None or \
            status.attributes['emoji'] is not None
        return r
    except AttributeError:
        return False


def clean_status(user):
    try:
        status = user.status.get()
    except AttributeError:
        return
    status.message = ""
    status.emoji = ""
    try:
        status.save()
    except AttributeError:
        return


#
# Filter for finding old users that have been idle for longer than time period
#
def is_recently_active(user, time):
    # actively used the system
    last_activity_on = user_ptime(user, 'last_activity_on', fmt_iso_date)
    if after(last_activity_on, time):
        return True

    # did they even login?
    last_sign_in_at = user_ptime(user, 'last_sign_in_at', fmt_iso_time)
    if after(last_sign_in_at, time):
        return True

    return False


def on_blacklist(user):
    return user.attributes['name'] in blacklist_names


#
# convenience funcs to avoid double-negative logic downstream
#
def is_idle(user):
    if (is_greylisted(user)):
        itime = grey_idle_time
    else:
        itime = idle_time

    return (not is_recently_created(user, itime) and
            not is_recently_active(user, itime)) or \
            on_blacklist(user)


def is_zombie(user):
    if (is_greylisted(user)):
        ktime = grey_kill_time
    else:
        ktime = kill_time

    return not is_recently_active(user, ktime)


#
# convenience functions to avoid wrapping everything in try-except block
#

def block_user(user):
    try:
        user.block()
    except gitlab.exceptions.GitlabHttpError:
        pass


def delete_user(user):
    try:
        user.delete(hard_delete=True)
    except gitlab.exceptions.GitlabHttpError:
        pass


def ban_user(user):
    try:
        user.unblock()
    except gitlab.exceptions.GitlabHttpError:
        pass

    try:
        user.ban()
    except gitlab.exceptions.GitlabHttpError:
        pass


def active_user(user):
    try:
        return len(user.events.list(get_all=True)) > 0
    except gitlab.exceptions.GitlabHttpError:
        username = user.attributes['username']
        print(f"Error processing {username}")
        return True


#
# Main code start
#
users = get_all_users()
marked = filterfalse(is_good_user, users)
marked = filterfalse(is_whitelisted, marked)
marked = list(marked)

#
# Highlight bio's or websites that we don't know about
#
for user in marked:
    id = user.attributes['id']
    username = user.attributes['username']
    bio = user.attributes['bio']
    location = user.attributes['location']
    website_url = user.attributes['website_url']
    email = user.attributes['email']
    name = user.attributes['name']
    state = user.attributes['state']
    organization = user.attributes['organization']
    sanitize = False

    if (is_greylisted(user)):
        itime = grey_idle_time
    else:
        itime = idle_time

    # Bio's
    if bio is not None and bio.strip() != '' and \
       bio not in whitelist_bios:
        print(f"Bio for {username} is '{bio}'")
        sanitize = True

    if location is not None and location.strip() != '':
        print(f"Location for {username} is '{location}'")
        sanitize = True

    # Social Networks
    if has_social_network(user):
        sanitize = True

    # Organization
    if organization is not None and organization.strip() != '':
        sanitize = True

    # Websites
    if website_url is not None and website_url.strip() != '' and \
       website_url not in whitelist_urls:
        print(f"Website for {username} is '{website_url}'")
        sanitize = True

    if sanitize:
        if ci is not None:
            print(f"Sanitizing {username} ({state})")
        else:
            print(f"Sanitizing {name} <{email}> ({username}) ({state})")
        sanitize_user(user)

    # Status
    if has_status(user):
        if ci is not None:
            print(f"Sanitizing {username} status ({state})")
        else:
            print(f"Sanitizing {name} <{email}> ({username}) status ({state})")
        clean_status(user)


#
# Identify users that may not be active
#
idle = filter(is_idle, marked)

exit_code = 0

#
# Process the list of inactive users
#
for user in idle:

    email = user.attributes['email']
    name = user.attributes['name']
    username = user.attributes['username']
    state = user.attributes['state']
    bio = user.attributes['bio']
    location = user.attributes['location']
    website_url = user.attributes['website_url']

    action = ""
    delete = False
    block = False
    ban = False

    if (is_greylisted(user)):
        itime = grey_idle_time
        ktime = grey_kill_time
    else:
        itime = idle_time
        ktime = kill_time

    # Skip deactivated users
    if state == 'deactivated':
        continue

    # Determine what action to take
    if on_blacklist(user):
        action = f"DELETE # name '{user.name}' on the blacklist"
        ban = True
        delete = True
    elif user.attributes['last_sign_in_at'] is None:
        action = "DELETE # user never logged in and created before %s" % idle_time
        ban = True
        delete = True
    elif user.attributes['is_admin']:
        action = 'BLOCK # inactive admin'
        block = True
    elif is_zombie(user):
        action = "DELETE # no activity and created before %s" % ktime
        ban = True
        delete = True
    else:
        action = "BAN # no activity and created before %s" % itime
        ban = True

    if active_user(user) and state == 'blocked':
        continue

    # Don't do anything to accounts that have raised issues
    if active_user(user) and state != 'banned':
        if is_greylisted(user) and is_recently_created(user, ktime):
            print(f"WARNING: {name} ({username}) <{email}> ({state})",
                  f"should be inspected manually.")
            print(f"PROFILE: https://gitlab.isc.org/{username}")
            print(f"ADMIN  : https://gitlab.isc.org/admin/users/{username}")
            exit_code = 1
        continue

#    if block and state == 'blocked':
#        continue
#
#    if block and state == 'banned':
#        continue
#
#    if ban and state == 'banned':
#        continue

    # Debug output
    if not delete and not block and not ban:
        action = f"ERROR # run outside of the CI to see details"
        if ci is None:
            pprint(user.attributes)

    if ci is not None:
        print(f"Processing {username} ({state}): {action}")
    else:
        print(f"Processing {name} <{email}> ({username}) ({state}): {action}")

    # Block the users marked to be blocked
    if state != 'blocked' and state != 'banned' and block:
        block_user(user)

    # Ban the users marked to be banned
    if state != 'banned' and ban:
        ban_user(user)

    # Block users instead of deleting them as of this moment
    if delete and os.getenv("VICTOR_CLEANER"):
        delete_user(user)

sys.exit(exit_code)

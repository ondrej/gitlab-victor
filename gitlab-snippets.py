#!/usr/local/bin/python3

from datetime import datetime, timedelta
from pprint import pprint
from itertools import filterfalse
import gitlab
import os
import sys

cfg = os.getenv("PYTHON_GITLAB_CFG")
ci = os.getenv("CI")

gl = gitlab.Gitlab.from_config('default', [cfg])
gl.auth()

from pprint import pprint

def get_all_snippets():
    return gl.snippets.public(all=True)

#
# Main code start
#
snippets = get_all_snippets()
for snippet in snippets:
#    snippet = gl.snippets.get(snippet.attributes['id'])
    print(snippet.attributes['web_url'])
#    pprint(snippet.attributes)
#    pprint(snippet.content())
    
